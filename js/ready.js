$(document).ready(function(){
});
var oebps_dir = '';
var opf_file = '';
var ncx_file = '';

var currentBook = "";
var currentChap = "";
var currentPg = 1;
var pageCount = 1;

function dbConnect()
	{	
$.ajax({
    type: "POST",
  	url: './php/database.php?',  
	  async: false,
    data: {},
    cache: false,
    success: function(str)
        {
           // alert(str);
        }
    });	

	}
	
	function addCatagory()
	{
	
		$("#addCatagory").toggle(1000);


	}
	
function removeFolder(id)
{
	
var r=confirm("Are you sure you want to ?");
if (r==true)
  {
 $.ajax({
    type: "POST",
  	url: './php/removeFolder.php?',  
	  async: false,
    data: {'x':id},
    cache: false,
    success: function(str)
        {
           // alert(str);
        }
    });	
    
  document.location.reload(true);
  
  }
else
  {
  
  //x="You pressed Cancel!";
  } 
  
	
	
	
}
	
function catagoryClick(id)
{
 //alert(id);
 
 var y= document.getElementById("path");
		y.innerHTML = id;
		
var data = document.getElementById("path").innerHTML;
$("#formpath").val( data );		
$("#formpath2").val( data );	
		
	var x = document.getElementById("path").innerHTML;
	

		
		$.ajax({  
		type: 'POST',  
		   data: { 'x': x },
		url: './php/displayFolder.php?',  
		 async: false,
		success: function (data) 
		{
			
			var folders = document.getElementById("folders");
			folders.innerHTML = data;
			
			//alert(data);
			
		}
		
		});  //end ajax
 
}		 

	

function bookClick( name)
	{	currentBook = name;
		//alert("HERE4");
	
		$("#reader").show(1000);
		$("#bookList").hide(1000);
		$("#folders").hide(1000);
		//alert(name);
		updateDir(name);
		var path = epub_dir + '/META-INF/container.xml';
		//alert(path);
		jQuery.get(path, {}, container);
		//alert (path);
		$('#toc a').live('click', load_content);

	}


function removeBook(name)
	{	currentBook = name;
		
		
var dataString = currentBook ;

var r=confirm("Are you sure you want to delete this book?");
if (r==true)
  {
$.ajax({
    type: "POST",
  	url: 'removeBook.php?',  
	  async: false,
    data: { 'dataString': dataString },
     async: false,
    cache: false,
    success: function(str)
        {
	//alert(str);
        }
    });	

document.location.reload(true);
		
	}
	}


function addRemoveBook()
{

	$("#addRemoveBook").toggle(1000);
}


function updateDir(name)
{
//alert("HERE5");
var dir = epub_dir + '/' +name ;
epub_dir = dir;

}


function load_content() {
//alert("NOW");
	
			
    page = $(this).attr('href');
    currentChap = page;
pageCount = 1;
var x = document.getElementById("pageCounter");
		x.innerHTML = "Page:" + pageCount+" of this chapter";


// Unselect other sections
    $('.selected').attr('class', 'unselected');
    $(this).attr('class', 'selected');
    $('#content').load(page, null);
  //  updatePageCount();
  
    return false;
}


function load_content2(chap) {
//alert("NOW");
	
			
    page = chap;
    currentChap = page;
pageCount = 1;
var x = document.getElementById("pageCounter");
		x.innerHTML = "Page:" + pageCount+" of this chapter";


// Unselect other sections
    $('.selected').attr('class', 'unselected');
    $(this).attr('class', 'selected');
    $('#content').load(page, null);
  //  updatePageCount();
  
    return false;
}


function saveBookMark() 
{

 var dataString = currentBook +'~'+currentChap +'~' +pageCount;
//alert(currentChap);
$.ajax({
    type: "POST",
  	url: './php/saveBookMark.php?',  
	 async: false,
    data: { 'dataString': dataString },
    cache: false,
    success: function()
        {
            alert("Bookmark Saved");
        }
    });	
}


function addBook() 
{
$.ajax({
    type: "POST",
  	url: './php/addBook.php?',  
	 async: false,
    data: {  },
    cache: false,
    success: function()
        {
            //alert("Book Added");
        }
    });	
}


function goToBookMark() {
//alert("BookMark");
var input = "";
var name = "";
var chap ="";
var pg = "";

$.ajax({
    type: "POST",
  	url: './php/gotoBookmark.php?',  
    success: function(data)
        {	
	
	input = String(data);
	//alert(input);
	var count = 0;
	var i =0;
		while(input[i] )
	{
		if (input[i] == '~')
		{
			count++;
		}
		else if (count == 0)
		{
			name = name + input[i] ;
		}
		else if (count == 1)
		{
			chap = chap + input[i];
		}
		else if (count == 2)
		{
		pg = pg + input[i];
		}
		
		i++;
		
	}
	}
    });	




alert("Going to bookmark");
//alert(chap);
//alert(pg);


	bookClick( name);
	
 load_content2(chap);

    
    
    return false;
}

//container.xml
function container(f) {
	//alert("HERE");
    opf_file = $(f).find('rootfile').attr('full-path');               
    if (opf_file.indexOf('/') != -1) {
        oebps_dir = opf_file.substr(0, opf_file.lastIndexOf('/'));
    }
    opf_file = epub_dir + '/' + opf_file;
    jQuery.get(opf_file, {}, opf);
}

//toc.ncx
function toc(f) {
  //  alert("HERE2");
    $(f).find('navPoint').each(function() 
    {  
            var s = $('<span/>').text($(this).find('text:first').text());
            var a = $('<a/>').attr('href', epub_dir + '/' + oebps_dir + '/' + $(this).find('content').attr('src'));
            if ($(this).parent()[0].tagName.toLowerCase() == 'navpoint') {
              s.addClass('indent');
            }    
            s.appendTo(a);
            a.appendTo($('<li/>').appendTo('#toc'));
        });
	$('#toc a:eq(0)').click();

}

//content.opf
function opf(f) {
//alert("HERE3");
    var title = $(f).find('title').text();
    var author = $(f).find('creator').text();  
    $('#content-title').html(title + ' by ' + author);
    if (title == null || title == '') 
    {
        $('#content-title').html($(f).find('dc\\:title').text() + ' by ' +  $(f).find('dc\\:creator').text());
    }
    
    var opf_item_tag = 'opf\\:item';
    if ($(f).find('opf\\:item').length == 0) 
    {
       opf_item_tag = 'item';
    }

    $(f).find(opf_item_tag).each(function() 
    {
            if ( $(this).attr('href').indexOf('.ncx') != -1) {
                ncx_file = epub_dir + '/' + oebps_dir + '/' + $(this).attr('href');
                jQuery.get(ncx_file, {}, toc);
            }
        });
}

 $(document).ready(function(){
	
		$("#reader").hide();
		$("#bookList").show();
		$("#metadata").hide();
		$("#addRemoveBook").hide();
			$("#addCatagory").hide();
			$("#folders").show();
		
		
		
	
	
		
	var data = document.getElementById("path").innerHTML;
	
$("#formpath").val( data );
$("#formpath2").val( data );

	dbConnect();

		updatePageCount();
	//clearBooks();
	GetBooks();
	listBooks();
	Display();
	displayRoot();
	 
	 function listBooks()
	 {
		//alert("Listing Books");
	$.ajax({  
		type: 'POST',  
		url: './php/listBooks.php?',  
		 async: false,
		success: function (data) {
		document.getElementById("Books").innerHTML = data;

		}
		
		
	});  //end ajax
		 
	}
	 

$("#homeLink").click(function(){
	document.location.reload(true);
	});
	

$("#showMeta").click(function(){
	
	$("#metadata").toggle(1000);
});
	 
	 function clearBooks(){
		 
		//alert("removingBooks");
	$.ajax({  
		type: 'POST',  
		url: './php/removeFolders.php?',  
		 async: false,
		success: function (data) {}
	});  //end ajax
		 
	}
		 
	 
$("#changeToBlack").click(function(){
		var x = document.getElementById("rightPage");
		x.style.backgroundColor="black";
		x.style.color="white";
	 });
	 
	 
$("#changeToWhite").click(function(){
		var x = document.getElementById("rightPage");
		x.style.backgroundColor="white";
		x.style.color="black";
	 });
	 	
$("#smallSize").click(function(){
		var x = document.getElementById("rightPage");
		x.style.fontSize = "xx-small";
		
	 });		
	 
$("#medSize").click(function(){
		var x = document.getElementById("rightPage");
		x.style.fontSize = "medium";
		
	 });		
	 
$("#bigSize").click(function(){
		var x = document.getElementById("rightPage");
		x.style.fontSize = "xx-large";
		
	 });	

	 function updatePageCount()
	 {
		var x = document.getElementById("pageCounter");
		x.innerHTML = "Page:" + pageCount+" of this chapter";
		


	 }
	 

function displayRoot()
{
var x = document.getElementById("path").innerHTML;
	$.ajax({  
		type: 'POST',  
		   data: { 'x': x },
		url: './php/displayRoot.php?',  
		 async: false,
		success: function (data) 
		{
			
			var folders = document.getElementById("folders");
			//folders = "HELLLO";
			folders.innerHTML = data;
			
			//alert(data);
			
		}
		
		});  //end ajax

}
	 	 
	 
	function GetBooks(){
		
		//alert("Finding");
		$.ajax({  
		type: 'POST',  
		url: './php/getBooks.php?',  
		 async: false,
		success: function (data) {
			//$('#Books').html(data)
		//find div
			var form = document.getElementById("Books");
					
		//find div
			var div = document.createElement('div');

		//add text to div
		div.appendChild(document.createTextNode(data));

			//append div to form
			form.appendChild(div);
		}
		
		});  //end ajax
		
	}


	var n = $("#rightPage").height() - 10;
	
$('#next').click(function(event){
	if(pageCount > 0)
	{
		$('#rightPage').animate({ scrollTop: n },'10');
			pageCount++;
	updatePageCount();
	 n += $("#rightPage").height();
	}
		
});

$('#prev').click(function(event){
   if(pageCount > 1)
{
   pageCount--;
	updatePageCount();
	n -= $("#rightPage").height();
	$('#rightPage').animate({ scrollTop: n },'10');
}	 


});
	
	
	listContents();
	
	function listContents()
	{
		$.ajax({  
		type: 'POST',  
		 async: false,
		url: './php/listFiles.php?',  
		success: function (data) {
			
		
		 var metafiles = new Array();
		 count = 0;
		
			for( i =0; i < data.length; i++)
			{
				
				if ( data[i] == '%')
					count++
				else
				{
					metafiles[count] = metafiles[count] + data[i];
				}
			
			}
		
			for( k =0; k < count; k++)
			metaDisplay(metafiles[k]);
		}
		
		
		
		});  //end ajax
	}
	
	
	
	function Display(){
	
	
		//alert("Disaplying");
	$.ajax({  
		type: 'POST',  
		url: './php/disBooks.php?',  
		success: function (data) {
		$('#happy').html(data);
		}
		
		});  //end ajax
	}
	
	function metaDisplay(path)
	{
	


path = path.split("unZipped/").pop();
path = "unZipped" + path;
	
	
	
		if (window.XMLHttpRequest) 
		{ 
		    xmlhttp=new XMLHttpRequest(); 
		} 
		else 
		{ 
		    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP"); 
		} 
		
		xmlhttp.open("GET",path,false); 
		xmlhttp.send(); 
		xmlDoc=xmlhttp.responseXML; 

		names = path;

		names = names.split("unZipped/").pop();
		names = names.split("/").slice();
		//alert(names[0]);
		
		
		 x = xmlDoc.getElementsByTagName("metadata");
			//alert(x.textConent);
		var d = document.getElementById("metadata");
		for ( i = 0; i < x.length; i++ ) 
		{ 	
			d.innerHTML = d.innerHTML + "</br> Title: " +(x[i].getElementsByTagName("dc:title")[0].childNodes[0].nodeValue) ; 
			var z = document.getElementById(names[0]);
				z.innerHTML = (x[i].getElementsByTagName("dc:title")[0].childNodes[0].nodeValue);
			//d.innerHTML = d.innerHTML + "</br>Rights: " +(x[i].getElementsByTagName("dc:rights")[0].childNodes[0].nodeValue); 
			//d.innerHTML = d.innerHTML + "</br>Identifier: " +(x[i].getElementsByTagName("dc:identifier")[0].childNodes[0].nodeValue); 
			//d.innerHTML = d.innerHTML + "</br>Creator: " +(x[i].getElementsByTagName("dc:creator")[0].childNodes[0].nodeValue);
			d.innerHTML = d.innerHTML + "</br>Language: " +(x[i].getElementsByTagName("dc:language")[0].childNodes[0].nodeValue); 
			//d.innerHTML = d.innerHTML + "</br>Subject: " +(x[i].getElementsByTagName("dc:subject")[0].childNodes[0].nodeValue); 
			d.innerHTML = d.innerHTML + "</br>Date: " +(x[i].getElementsByTagName("dc:date")[0].childNodes[0].nodeValue) + " </br>"; 
			//d.innerHTML = d.innerHTML + "</br>Source: " +(x[i].getElementsByTagName("dc:source")[0].childNodes[0].nodeValue); 
			//d.innerHTML = d.innerHTML + "</br>Rights: " +(x[i].getElementsByTagName("dc:")[0].childNodes[0].nodeValue); 
			
		
		}
}
	
	
	

});//end Ready